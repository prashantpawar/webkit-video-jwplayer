### For MP4 Playback:

* Download https://github.com/Soundnode/soundnode-app/blob/master/assets/codecs-libs/ffmpegsumo.so?raw=true

* In resources/node-webkit/MacOS32/ 
	* Right-click on `node-webkit`
	* Select `Show Package Contents`

* Replace the file of the same name (or add if missing) in Contents/Frameworks/Libraries