/**
 * Directive to embed a video using JW player
 * attributes:
 * - width
 * - height
 * - source (passed through assetUrlBuilder)
 * - poster image
 */

app.directive('jwplayer', ['$log', '$timeout', function ($log, $timeout) {
    'use strict';
    return {
        restrict: 'E',
        templateUrl: './jwplayer/jwplayer.html',
        scope: {
            source: '=',
            cover: '=',
            transcript: '='
        },
        link: function ($scope) {
            /**
             * Generate id to use as an id for JWPlayer element.
             * (JWPlayer needs a unique id to render itself)
             */
            var generateId = function (source) {
                if (source) {
                    var d = new Date();
                    var file = source.substr(source.lastIndexOf('/') + 1);
                    var extension = file.substr(file.indexOf('.'), file.length);
                    var id = file.substr(0, file.indexOf(extension)).toLowerCase() + d.getTime();
                    return id || '';
                }
            };

            /**
             * When we generated unique id based on title,
             * attach it to the player element.
             */
            $scope.id = generateId($scope.source);

            /* make sure last part of url is not null, otherwise do not show transcript */
            $scope.transcriptExist = true;
            if ($scope.transcript) {
                var transcriptComponents = $scope.transcript.split('/');
                $scope.transcriptExist = transcriptComponents[transcriptComponents.length - 1] !== 'null';
            }

            /**
             * JW Player requires a licence to hide its logo.
             * Set that licence key here if not already set.
             */
            if (!jwplayer.key) {
                jwplayer.key = SapphireConfig.jwPlayerKey;
            }

            $scope.$on('$destroy', function () {
                if (jwplayer($scope.id)) {
                    jwplayer($scope.id).file = '';
                    jwplayer($scope.id).image = '';
                    angular.element('jwplayer').remove();
                }
            });


            /**
             * If there is no source, don't bother
             * initializing JW Player.
             */
            if (!$scope.source) {
                $scope.message = 'Problem loading video';
            } else {
                try {
                    /**
                     * $watch is needed here because
                     * - JWPlayer needs id to initialize
                     * - We generate ID above, and cycle needs to go to apply it to DOM element
                     */
                    $timeout(function () {
                        jwplayer($scope.id).setup({
                            width: '100%',
                            aspectratio: '16:9',
                            file: $scope.source,
                            image: $scope.cover,
                            analytics: { enabled: false },
                            events: {
                                onError: function () {
                                    console.log('Error loading player');
                                    jwplayer($scope.id).file = '';
                                    jwplayer($scope.id).image = '';
                                    jwplayer($scope.id).remove();
                                },
                                onSetupError: function () {
                                    console.log('Error loading player');
                                    jwplayer($scope.id).file = '';
                                    jwplayer($scope.id).image = '';
                                    jwplayer($scope.id).remove();
                                }
                            }
                        });
                    }, 100);
                } catch (e) {
                }
            }


            /**
             * Callback on clicking transcript link.
             */
            $scope.onTranscriptClick = function (event) {
                if (!navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
                    event.currentTarget.target = '';
                }
            };
        }
    };
}]);
